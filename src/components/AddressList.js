import React from "react";
import {Address} from "./Address"

export const AddressList = (props) =>
{

    return (
        <div>
            {props.addresses.map(address => (
                <Address key={address.id} address={address} deleteAddress={props.deleteAddress}/>
            ))}
        </div>
    );
};
