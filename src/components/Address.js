import React from "react";

export const Address = (props) =>
{

    let address = props.address;

    return (
        <div className="address-item">
            <div className="container-fluid">
                <div className="row">
                    <div className="address-item-name pull-left">
                        {address.firstName} {address.lastName}
                    </div>
                    <br></br>
                </div>
                <div className="row">
                    <span>{address.addressLine}, </span>
                    <span>{address.city}, </span>
                </div>
                <div className="row">
                    <span>{address.postCode}, </span>
                    <span>{address.state}</span>
                </div>
                <div className="row">
                    <div className="pull-right">
                        <button className="btn btn-danger"
                                onClick={() => props.deleteAddress(address)}>
                            Remove Address
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
};
