import React from "react";

export class NewAddress extends React.Component {

    constructor(props)
    {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            addressLine: '',
            city: '',
            postCode: '',
            state: ''
        };
    }

    handleChange(e)
    {
        e.target.classList.add('active');

        this.setState({
            [e.target.name]: e.target.value
        });

        // this.showInputError(e.target.name);
    }

    render()
    {
        return (
            <form>
                <div className="form-group row">
                    <div className="col-md-6">
                        <input type="text"
                               className="form-control"
                               placeholder="First Name"
                               name="firstName"
                               required
                               value={this.state.firstName}
                               onChange={this.handleChange.bind(this)}
                        />
                        {
                            (this.state.firstName.length <= 0) &&
                            <div className="alert alert-danger"
                            >
                                <div>
                                    First Name is required
                                </div>
                            </div>
                        }
                    </div>
                    <div className="col-md-6">
                        <input type="text"
                               className="form-control"
                               placeholder="Last Name"
                               name="lastName"
                               required
                               value={this.state.lastName}
                               onChange={this.handleChange.bind(this)}
                        />
                        {
                            this.state.lastName.length <= 0 &&
                            <div className="alert alert-danger"
                            >
                                <div>
                                    Last Name is required
                                </div>
                            </div>
                        }
                    </div>
                </div>
                <div className="form-group row">
                    <div className="col-md-12">
                        <input type="text"
                               className="form-control"
                               placeholder="Address Line"
                               name="addressLine"
                               required
                               value={this.state.addressLine}
                               onChange={this.handleChange.bind(this)}
                        />
                        {
                            this.state.addressLine.length <= 0 &&
                            <div className="alert alert-danger"
                            >
                                <div>
                                    Address Line is required
                                </div>
                            </div>
                        }
                    </div>
                </div>
                <div className="form-group row">
                    <div className="col-md-6">
                        <input type="text"
                               className="form-control"
                               placeholder="City"
                               name="city"
                               required
                               value={this.state.city}
                               onChange={this.handleChange.bind(this)}
                        />
                        {
                            this.state.city.length <= 0 &&
                            <div className="alert alert-danger"
                            >
                                <div>
                                    City is required
                                </div>
                            </div>
                        }
                    </div>
                    <div className="col-md-6">
                        <input type="text"
                               className="form-control"
                               placeholder="Postcode"
                               name="postCode"
                               required
                               value={this.state.postCode}
                               onChange={this.handleChange.bind(this)}
                        />
                        {
                            this.state.postCode.length <= 0 &&
                            <div className="alert alert-danger">
                                <div>
                                    Post Code is required
                                </div>
                                <div>
                                    Post Code must be at least 5 characters long.
                                </div>
                                <div>
                                    Post Code cannot be more than 5 characters long.
                                </div>
                            </div>
                        }
                    </div>
                </div>
                <div className="form-group row">
                    <div className="col-md-10">
                        <input type="text"
                               className="form-control"
                               placeholder="State"
                               name="state"
                               required
                               value={this.state.state}
                               onChange={this.handleChange.bind(this)}
                        />
                        {
                            this.state.state.length <= 0 &&
                            <div className="alert alert-danger"
                            >
                                <div>
                                    State is required
                                </div>
                            </div>
                        }
                    </div>
                    <div className="col-md-2">
                        <button
                            type="button"
                            className="btn btn-success pull-right"
                            onClick={() => this.props.createAddress(this.state)}
                        >Add Address
                        </button>
                    </div>
                </div>
            </form>
        )
    }
}