const addressReducer = (state = {
    addresses: []
}, action) =>
{
    switch (action.type) {
        // case "GET_ADDRESSES":
        //     state = {
        //         ...state
        //     };
        //     break;
        case "GET_ADDRESSES_FULFILLED":
            state = {
                ...state,
                addresses: action.payload.data.addresses
            };
            break;
        case "CREATE_ADDRESS_FULFILLED":
            state = {
                ...state
            };
            break;
        case "CREATE_ADDRESS_REJECTED":
            state = {
                ...state,
                error: action.payload.message
            };
            break;
        case "DELETE_ADDRESS_FULFILLED":
            state = {
                ...state
            };
            break;
        case "DELETE_ADDRESS_REJECTED":
            state = {
                ...state,
                error: action.payload.message
            };
            break;
    }
    return state;
};

export default addressReducer;