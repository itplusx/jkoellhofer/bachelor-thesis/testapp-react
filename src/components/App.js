import React from "react";
import {connect} from "react-redux";

import {AddressList} from "./AddressList";
import {NewAddress} from "./NewAddress";
import {getAddresses, deleteAddress, createAddress} from "../actions/addressActions";

class App extends React.Component {

    componentDidMount()
    {
        this.props.getAddresses()
    }

    render()
    {
        return (
            <div className="container">
                <h1 class="text-center">{title}</h1>
                <div class="row">
                    <div class="col-md-12">
                        <AddressList addresses={this.props.addresses} deleteAddress={this.props.deleteAddress}/>
                        <NewAddress createAddress={this.props.createAddress}/>
                    </div>
                </div>
            </div>
        );
    }
}

const title = "Fictional Address List: React";

const mapStateToProps = (state) =>
{
    return {
        addresses: state.address.addresses
    };
};

const mapDispatchToProps = (dispatch) =>
{
    return {
        getAddresses: () =>
        {
            dispatch(getAddresses());
        },
        deleteAddress: (address) =>
        {
            dispatch(deleteAddress(address))
                .then(
                    response =>
                    {
                        dispatch(getAddresses());
                    }
                )
                .catch(
                    error => console.log(error)
                );
        },
        createAddress: (address) =>
        {
            dispatch(createAddress(address))
                .then(
                    response =>
                    {
                        dispatch(getAddresses());
                    }
                )
                .catch(
                    error => console.log(error)
                );
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
