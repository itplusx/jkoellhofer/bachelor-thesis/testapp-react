import axios from "axios";

export function getAddresses()
{
    return {
        type: "GET_ADDRESSES",
        payload: axios.get('http://testapp-api.dev/api/addresses')
    };
}

export function deleteAddress(address)
{
    return {
        type: "DELETE_ADDRESS",
        payload: axios.delete('http://testapp-api.dev/api/addresses/' + address.id)
    };
}

export function createAddress(address)
{
    return {
        type: "CREATE_ADDRESS",
        payload: axios.post('http://testapp-api.dev/api/addresses', address)
    };
}