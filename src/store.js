import {createStore, combineReducers, applyMiddleware} from "redux";
import logger from "redux-logger";
import thunk from "redux-thunk";
import promise from "redux-promise-middleware";

import address from "./reducers/addressReducer";

export default createStore(
    combineReducers({
        address
    }),
    {},
    applyMiddleware(logger(), thunk, promise())
);